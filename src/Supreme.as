package {
	import flash.display.Sprite;
	import flash.events.Event;
	import game.factories.AnimationsFac;
	import game.main_menu.MainMenu;
	import game.managing.BitmapStorage;
	import game.World;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public class Supreme extends Sprite {
		
		private var world:World;
		private var mm:MainMenu;
		
		private var bmdStorage:BitmapStorage;
		private var animFactory:AnimationsFac;
		
		private var stepTrigger:Boolean = false;
		
		public function Supreme():void {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			bmdStorage = new BitmapStorage();
			animFactory = AnimationsFac.initCopy();
			
			mm = new MainMenu(bmdStorage);
			addChild(mm);
			mm.init(false);
			mm.onPlay = fromMMtoWorld;
			
			stage.addEventListener(Event.ENTER_FRAME, ef);
		}
		private function fromMMtoWorld(_niceGraphix:Boolean):void {
			mm.dispose();
			removeChild(mm);
			mm.onPlay = null;
			mm = null;
			
			world = new World(bmdStorage);
			addChild(world);
			world.init(_niceGraphix);
			world.onGameOver = fromWorldtoMM;
			
			stepTrigger = true;
		}
		private function fromWorldtoMM(_niceGraphix:Boolean):void {
			world.dispose();
			removeChild(world);
			world.onGameOver = null;
			world = null;
			
			mm = new MainMenu(bmdStorage);
			addChild(mm);
			mm.init(_niceGraphix);
			mm.onPlay = fromMMtoWorld;
			
			stepTrigger = false;
		}
		private function ef(e:Event):void {
			if (stepTrigger) world.step();
			else mm.step();
			animFactory.step();
		}
	}
}