package game.factories {
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class MathPlushies {
		
		public static function getRandom(_from:Number, _to:Number):Number {
			return _from + (_to - _from) * Math.random();
		}
	}
}