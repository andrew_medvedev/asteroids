package game.factories {
	import flash.display.Shape;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class ShapeFactory {
		static const SHIP_SCALE_MUL:Number = 4;
		static const SHIP_X_DEVIANT:Number = 3;
		static const SHIP_Y_DEVIANT:Number = 3.5;
		
		static const GUI_SHIP_SCALE_MUL:Number = 1.5;
		
		static const UFO_SCALE_MUL:Number = 4;
		static const UFO_X_DEVIANT:Number = 6;
		static const UFO_Y_DEVIANT:Number = 3;
		
		private static var ast1Pnts:Vector.<Point> = new <Point>[new Point(2, 0),
																 new Point(6, 2),
																 new Point(8, 0),
																 new Point(10, 1),
																 new Point(9, 3),
																 new Point(10, 7),
																 new Point(7, 10),
																 new Point(2, 10),
																 new Point(0, 8),
																 new Point(0, 2),
																 new Point(2, 0)];
		private static var ast1PntsLength:int = ast1Pnts.length;
		private static var ast1XDeviant:Number = 5;
		private static var ast1YDeviant:Number = 5;
		
		private static var ast2Pnts:Vector.<Point> = new <Point>[new Point(4, 0),
																 new Point(6, 2),
																 new Point(9, 0),
																 new Point(12, 3),
																 new Point(9, 4),
																 new Point(11, 7),
																 new Point(7, 11),
																 new Point(3, 8),
																 new Point(2, 10),
																 new Point(0, 7),
																 new Point(3, 5),
																 new Point(1, 3),
																 new Point(4, 0)];
		private static var ast2PntsLength:int = ast2Pnts.length;
		private static var ast2XDeviant:Number = 6;
		private static var ast2YDeviant:Number = 5.5;
		
		private static var ast3Pnts:Vector.<Point> = new <Point>[new Point(4, 0),
																 new Point(7, 0),
																 new Point(9, 3),
																 new Point(9, 5),
																 new Point(5, 9),
																 new Point(3, 9),
																 new Point(4, 5),
																 new Point(1, 8),
																 new Point(0, 6),
																 new Point(2, 4),
																 new Point(0, 3),
																 new Point(4, 0)];
		private static var ast3PntsLength:int = ast3Pnts.length;
		private static var ast3XDeviant:Number = 4.5;
		private static var ast3YDeviant:Number = 4.5;
		
		private static var ast4Pnts:Vector.<Point> = new <Point>[new Point(2, 0),
																 new Point(5, 0),
																 new Point(9, 3),
																 new Point(9, 4),
																 new Point(6, 5),
																 new Point(9, 7),
																 new Point(7, 9),
																 new Point(5, 8),
																 new Point(2, 9),
																 new Point(0, 6),
																 new Point(0, 2),
																 new Point(3, 2),
																 new Point(2, 0)];
		private static var ast4PntsLength:int = ast4Pnts.length;
		private static var ast4XDeviant:Number = 4.5;
		private static var ast4YDeviant:Number = 5;
		
		private static var _r:int;
		private static var _i:int;
		
		public static function drawShip(_shape:Shape):void {
			_shape.graphics.lineStyle(1, 0xffffff);
			_shape.graphics.moveTo((-SHIP_X_DEVIANT) * SHIP_SCALE_MUL, (7 - SHIP_Y_DEVIANT) * SHIP_SCALE_MUL);
			_shape.graphics.lineTo((3 - SHIP_X_DEVIANT) * SHIP_SCALE_MUL, (-SHIP_Y_DEVIANT) * SHIP_SCALE_MUL);
			_shape.graphics.lineTo((6 - SHIP_X_DEVIANT) * SHIP_SCALE_MUL, (7 - SHIP_Y_DEVIANT) * SHIP_SCALE_MUL);
			_shape.graphics.moveTo((1 - SHIP_X_DEVIANT) * SHIP_SCALE_MUL, (5 - SHIP_Y_DEVIANT) * SHIP_SCALE_MUL);
			_shape.graphics.lineTo((5 - SHIP_X_DEVIANT) * SHIP_SCALE_MUL, (5 - SHIP_Y_DEVIANT) * SHIP_SCALE_MUL);
		}
		public static function drawGuiShip(_shape:Shape):void {
			_shape.graphics.lineStyle(1, 0xffffff);
			_shape.graphics.moveTo(0, 7 * GUI_SHIP_SCALE_MUL);
			_shape.graphics.lineTo(3 * GUI_SHIP_SCALE_MUL, 0);
			_shape.graphics.lineTo(6 * GUI_SHIP_SCALE_MUL, 7 * GUI_SHIP_SCALE_MUL);
			_shape.graphics.moveTo(GUI_SHIP_SCALE_MUL, 5 * GUI_SHIP_SCALE_MUL);
			_shape.graphics.lineTo(5 * GUI_SHIP_SCALE_MUL, 5 * GUI_SHIP_SCALE_MUL);
		}
		public static function drawUFO(_shape:Shape):void {
			_shape.graphics.lineStyle(1, 0xffffff);
			_shape.graphics.moveTo(( -UFO_X_DEVIANT) * UFO_SCALE_MUL, (4 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((4 - UFO_X_DEVIANT) * UFO_SCALE_MUL, (2 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((5 - UFO_X_DEVIANT) * UFO_SCALE_MUL, ( -UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((7 - UFO_X_DEVIANT) * UFO_SCALE_MUL, ( -UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((8 - UFO_X_DEVIANT) * UFO_SCALE_MUL, (2 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((12 - UFO_X_DEVIANT) * UFO_SCALE_MUL, (4 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((9 - UFO_X_DEVIANT) * UFO_SCALE_MUL, (6 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((3 - UFO_X_DEVIANT) * UFO_SCALE_MUL, (6 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo(( -UFO_X_DEVIANT) * UFO_SCALE_MUL, (4 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((12 - UFO_X_DEVIANT) * UFO_SCALE_MUL, (4 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.moveTo((4 - UFO_X_DEVIANT) * UFO_SCALE_MUL, (2 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
			_shape.graphics.lineTo((8 - UFO_X_DEVIANT) * UFO_SCALE_MUL, (2 - UFO_Y_DEVIANT) * UFO_SCALE_MUL);
		}
		public static function drawAsteroid(_shape:Shape, _scaleMul:Number = 1):void {
			_shape.graphics.lineStyle(1, 0xffffff);
			switch(_r = int(Math.random() * 4)) {
				case 0:
					_shape.graphics.moveTo((ast1Pnts[0].x - ast1XDeviant) * _scaleMul, (ast1Pnts[0].y - ast1YDeviant) * _scaleMul);
					for (_i = 1; _i < ast1PntsLength; _i++ )
						_shape.graphics.lineTo((ast1Pnts[_i].x - ast1XDeviant) * _scaleMul, (ast1Pnts[_i].y - ast1YDeviant) * _scaleMul);
					break;
				case 1:
					_shape.graphics.moveTo((ast2Pnts[0].x - ast2XDeviant) * _scaleMul, (ast2Pnts[0].y - ast2YDeviant) * _scaleMul);
					for (_i = 1; _i < ast2PntsLength; _i++ )
						_shape.graphics.lineTo((ast2Pnts[_i].x - ast2XDeviant) * _scaleMul, (ast2Pnts[_i].y - ast2YDeviant) * _scaleMul);
					break;
				case 2:
					_shape.graphics.moveTo((ast3Pnts[0].x - ast3XDeviant) * _scaleMul, (ast3Pnts[0].y - ast3YDeviant) * _scaleMul);
					for (_i = 1; _i < ast3PntsLength; _i++ )
						_shape.graphics.lineTo((ast3Pnts[_i].x - ast3XDeviant) * _scaleMul, (ast3Pnts[_i].y - ast3YDeviant) * _scaleMul);
					break;
				case 3:
					_shape.graphics.moveTo((ast4Pnts[0].x - ast4XDeviant) * _scaleMul, (ast4Pnts[0].y - ast4YDeviant) * _scaleMul);
					for (_i = 1; _i < ast4PntsLength; _i++ )
						_shape.graphics.lineTo((ast4Pnts[_i].x - ast4XDeviant) * _scaleMul, (ast4Pnts[_i].y - ast4YDeviant) * _scaleMul);
					break;
			}
		}
	}
}