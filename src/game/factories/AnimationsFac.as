﻿package game.factories {
	
	public class AnimationsFac {
		
		private static var copy:AnimationsFac;
		
		private var packs:Vector.<AnimationPack>;
		
		private var _i:int;

		public function AnimationsFac() {
			packs = new Vector.<AnimationPack>;
		}
		public function getPack(_smooth:Boolean = true):AnimationPack {
			var _out:AnimationPack = new AnimationPack(_smooth);
			packs[packs.length] = _out;
			return _out;
		}
		public function disposePack(_pack:AnimationPack):void {
			for (var i:int = 0; i < packs.length; i++ )
				if (packs[i] == _pack) {
					packs.splice(i, 1);
					return;
				}
		}
		public static function staticGetPack(_smooth:Boolean = true):AnimationPack {
			if (copy)
				return copy.getPack(_smooth);
			else return null;
		}
		public static function staticDisposePack(_pack:AnimationPack):void {
			if (copy) copy.disposePack(_pack);
		}
		public static function initCopy():AnimationsFac {
			if (!copy) {
				copy = new AnimationsFac();
				return copy;
			} else return null;
		}
		public function step():void {
			for (_i = 0; _i < packs.length; _i++ )
				packs[_i].step();
		}
	}
}
