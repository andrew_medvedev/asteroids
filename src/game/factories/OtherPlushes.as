package game.factories {
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class OtherPlushes {
		
		public static var textFormat_18:TextFormat = new TextFormat("Calibri", 18, 0xffffff, false);
		public static var textFormat_12:TextFormat = new TextFormat("Calibri", 12, 0xffffff, false);
		public static var textFormat_30:TextFormat = new TextFormat("Calibri", 30, 0xffffff, false);
		public static var textFormat_70:TextFormat = new TextFormat("Calibri", 70, 0xffffff, false);
		
	}
}