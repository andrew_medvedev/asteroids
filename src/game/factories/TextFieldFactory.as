package game.factories {
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class TextFieldFactory {
		
		public static function setupTF(_tf:TextField, _text:String, _format:TextFormat):void {
			_tf.defaultTextFormat = _format;
			_tf.setTextFormat(_format);
			_tf.text = _text;
			_tf.width = _tf.textWidth + 5;
			_tf.height = _tf.textHeight + 5;
			_tf.type = "dynamic";
			_tf.selectable = false;
		}
	}
}