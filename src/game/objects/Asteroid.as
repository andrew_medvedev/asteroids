package game.objects {
	import flash.display.Bitmap;
	import flash.display.Shape;
	import game.factories.MathPlushies;
	import game.factories.ShapeFactory;
	import game.managing.BitmapStorage;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class Asteroid extends Entity {
		public static const LARGE:int = 3;
		public static const MEDIUM:int = 2;
		public static const THE_TINY:int = 1;
		
		static const LARGE_SPEED_FROM:Number = 1;
		static const LARGE_SPEED_TO:Number = 2;
		static const MEDIUM_SPEED_FROM:Number = 2;
		static const MEDIUM_SPEED_TO:Number = 3;
		static const THE_TINY_SPEED_FROM:Number = 3;
		static const THE_TINY_SPEED_TO:Number = 4;
		
		public var active:Boolean = false;
		private var onInactiveCallback:Function;
		
		public var graphix:Bitmap;
		public var shape:Shape;
		
		public var size:int = 0;
		
		private var movVectorX:Number = 0;
		private var movVectorY:Number = 0;
		
		public var direction:Number = 0;
		
		private var bmds:BitmapStorage;
		
		public function Asteroid(_onInactiveCallback:Function, _bmds:BitmapStorage) {
			super(0);
			graphix = new Bitmap(null, "auto", true);
			addChild(graphix);
			graphix.visible = false;
			shape = new Shape();
			addChild(shape);
			this.visible = false;
			
			bmds = _bmds;
			onInactiveCallback = _onInactiveCallback;
		}
		public function mobilize(_size:int, _rotation:Number):void {
			graphix.bitmapData = bmds.AsteroidBmd;
			size = _size;
			var _sp:Number;
			switch(size) {
				case LARGE:
					this.radius = 30;
					_sp = MathPlushies.getRandom(LARGE_SPEED_FROM, LARGE_SPEED_TO);
					ShapeFactory.drawAsteroid(shape, 6);
					graphix.scaleX = graphix.scaleY = 1;
					break;
				case MEDIUM:
					this.radius = 18;
					_sp = MathPlushies.getRandom(MEDIUM_SPEED_FROM, MEDIUM_SPEED_TO);
					ShapeFactory.drawAsteroid(shape, 4);
					graphix.scaleX = graphix.scaleY = .66;
					break;
				case THE_TINY:
					this.radius = 12;
					_sp = MathPlushies.getRandom(THE_TINY_SPEED_FROM, THE_TINY_SPEED_TO);
					ShapeFactory.drawAsteroid(shape, 2.2);
					graphix.scaleX = graphix.scaleY = .38;
					break;
			}
			graphix.x = -graphix.width * .5;
			graphix.y = -graphix.height * .5;
			movVectorX = Math.cos(_rotation * Math.PI / 180) * _sp;
			movVectorY = Math.sin(_rotation * Math.PI / 180) * _sp;
			active = this.visible = true;
			direction = _rotation;
		}
		public function destroy():void {
			shape.graphics.clear();
			active = this.visible = false;
			onInactiveCallback(this);
		}
		public override function step():void {
			if (active) {
				this.x += movVectorX;
				this.y += movVectorY;
				if (this.x < 0) this.x = stage.stageWidth;
				if (this.x > stage.stageWidth) this.x = 0;
				if (this.y < 0) this.y = stage.stageHeight;
				if (this.y > stage.stageHeight) this.y = 0;
			}
		}
	}
}