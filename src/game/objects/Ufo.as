package game.objects {
	import flash.display.BitmapData;
	import flash.display.Shape;
	import game.factories.AnimationPack;
	import game.factories.AnimationsFac;
	import game.factories.ShapeFactory;
	import game.managing.BitmapStorage;
	import game.managing.LinksHolder;
	import game.pools.UfoBulletsPool;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class Ufo extends Entity {
		static const SPEED:Number = 4;
		static const SHOOTING_DEVIANT:Number = 15;
		
		public var graphix:AnimationPack;
		public var shape:Shape;
		
		public var active:Boolean = false;
		
		private var direction:Number = 0;
		
		private var life:Number = 1;
		private var lifeDecr:Number = .0015;
		
		private var rotReloading:Number = 0;
		private var rotReloadingSpeed:Number = .03;
		
		private var links:LinksHolder;
		
		private var fireReloading:Number = 0;
		private var fireReloadingSpeed:Number = .1;
		
		private var movX:Number;
		private var movY:Number;
		
		public function Ufo(_links:LinksHolder) {
			super(14);
			
			links = _links;
			
			graphix = AnimationsFac.staticGetPack(false);
			graphix.addAnimationSheet(new <BitmapData>[links.bmdStorage.ufo1,
													   links.bmdStorage.ufo2,
													   links.bmdStorage.ufo3,
													   links.bmdStorage.ufo4,
													   links.bmdStorage.ufo5,
													   links.bmdStorage.ufo6,
													   links.bmdStorage.ufo7,
													   links.bmdStorage.ufo8,
													   links.bmdStorage.ufo9,
													   links.bmdStorage.ufo10,
													   links.bmdStorage.ufo11,
													   links.bmdStorage.ufo12], "mn", 20);
			graphix.CurrentSheet = "mn";
			graphix.play = false;
			graphix.visible = false;
			addChild(graphix);
			shape = new Shape();
			addChild(shape);
			this.visible = false;
			ShapeFactory.drawUFO(shape);
		}
		public function mobilize():void {
			life = 1;
			rotReloading = 0;
			direction = Math.round(Math.random() * (360 / 45)) * 45;
			movX = Math.cos(direction * Math.PI / 180) * SPEED;
			movY = Math.sin(direction * Math.PI / 180) * SPEED;
			graphix.play = graphix.visible;
			active = this.visible = true;
		}
		public function destroy():void {
			active = this.visible = graphix.play = false;
		}
		public override function step():void {
			if (active) {
				if (links.ship.alive) {
					fireReloading += fireReloadingSpeed;
					if (fireReloading >= 1) {
						fireReloading--;
						links.ufoBulletsPool.mobXTrigger = this.x;
						links.ufoBulletsPool.mobYTrigger = this.y;
						links.ufoBulletsPool.mobRotTrigger = getAngle(this.x, this.y, links.ship.x, links.ship.y) - SHOOTING_DEVIANT + Math.random() * SHOOTING_DEVIANT * 2;
						links.ufoBulletsPool.mobilizeBullet();
					}
				}
				rotReloading += rotReloadingSpeed;
				if (rotReloading >= 1) {
					rotReloading--;
					if (Math.round(Math.random())) direction += 45;
					else direction -= 45;
					movX = Math.cos(direction * Math.PI / 180) * SPEED;
					movY = Math.sin(direction * Math.PI / 180) * SPEED;
				}
				life -= lifeDecr;
				if (life <= 0)
					destroy();
				this.x += movX;
				this.y += movY;
				if (this.x < 0) this.x = stage.stageWidth;
				if (this.x > stage.stageWidth) this.x = 0;
				if (this.y < 0) this.y = stage.stageHeight;
				if (this.y > stage.stageHeight) this.y = 0;
			}
		}
		private function getAngle(_x1:Number, _y1:Number, _x2:Number, _y2:Number):Number {
			var _dx:Number = _x2 - _x1;
			var _dy:Number = _y2 - _y1;
			return Math.atan2(_dy, _dx) * 180 / Math.PI;
		}
	}
}