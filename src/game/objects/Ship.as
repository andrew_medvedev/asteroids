package game.objects {
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.geom.Point;
	import game.factories.ShapeFactory;
	import game.lays.GuiLay;
	import game.managing.BitmapStorage;
	import game.managing.LinksHolder;
	import game.pools.ShipBulletsPool;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class Ship extends Entity {
		static const ROT_SPEED:Number = 8;
		static const MAX_SPEED:Number = 15;
		static const FRICTION:Number = .95;
		static const ACCELERATION:Number = .3;
		
		public var graphix:Bitmap;
		public var shape:Shape;
		
		private var speedVector:Point;
		
		public var forward:Boolean = false;
		public var left:Boolean = false;
		public var right:Boolean = false;
		
		private var reloading:Number = 1;
		private var reloadingSpeed:Number = .5;
		private var reloadingTrigger:Boolean = false;
		public var fire:Boolean = false;
		
		private var links:LinksHolder;
		
		public var onNoLifes:Function;
		
		private var _speed:Number;
		
		public var alive:Boolean = true;
		private var aliveReloading:Number = 1;
		private var aliveReloadingSpeed:Number = .009;
		private var dead:Boolean = false;
		
		public function Ship(_links:LinksHolder) {
			super(8);
			graphix = new Bitmap(_links.bmdStorage.ship, "auto", true);
			addChild(graphix);
			graphix.rotation += 90;
			graphix.x = graphix.width * .5;
			graphix.y = -graphix.height * .5;
			graphix.visible = false;
			shape = new Shape();
			addChild(shape);
			shape.rotation += 90;
			ShapeFactory.drawShip(shape);
			speedVector = new Point();
			
			links = _links;
		}
		public function destroy():void {
			this.visible = fire = forward = left = right = reloadingTrigger = false;
			reloading = 1;
			alive = false;
			aliveReloading = 0;
			if (links.guiLay.lifes.destroyLife() == 0) {
				dead = true;
				onNoLifes();
			}
		}
		public override function step():void {
			if (alive) {
				if (reloadingTrigger) {
					reloading += reloadingSpeed;
					if (reloading >= 1)
						reloadingTrigger = false;
				} else 
					if (fire) {
						links.shipBulletsPool.mobXTrigger = this.x;
						links.shipBulletsPool.mobYTrigger = this.y;
						links.shipBulletsPool.mobRotTrigger = this.rotation;
						if (links.shipBulletsPool.mobilizeBullet()) {
							reloading -= 1;
							reloadingTrigger = true;
						}
					}
				stepMovement();
			} else if (!dead) {
				aliveReloading += aliveReloadingSpeed;
				if (aliveReloading >= 1) {
					alive = true;
					this.x = stage.stageWidth * .5;
					this.y = stage.stageHeight * .5;
					this.rotation = -90;
					this.visible = true;
					speedVector.x = 0;
					speedVector.y = 0;
				}
			}
		}
		private function stepMovement():void {
			if (left || right) {
				if (left && !right)
					this.rotation -= ROT_SPEED;
				if (!left && right)
					this.rotation += ROT_SPEED;
			}
			if (forward) {
				_speed = Math.sqrt(Math.abs(speedVector.x * speedVector.x) + Math.abs(speedVector.y * speedVector.y));
				speedVector.x += Math.cos(this.rotation * Math.PI / 180) * ACCELERATION;
				speedVector.y += Math.sin(this.rotation * Math.PI / 180) * ACCELERATION;
				if (_speed > MAX_SPEED) {
					speedVector.x *= (MAX_SPEED / _speed);
					speedVector.y *= (MAX_SPEED / _speed);
				}
			} else {
				speedVector.x *= FRICTION;
				speedVector.y *= FRICTION;
				if (Math.abs(speedVector.x) < .1) speedVector.x = 0;
				if (Math.abs(speedVector.y) < .1) speedVector.y = 0;
			}
			this.x += speedVector.x;
			this.y += speedVector.y;
			if (this.x < 0) this.x = stage.stageWidth;
			if (this.x > stage.stageWidth) this.x = 0;
			if (this.y < 0) this.y = stage.stageHeight;
			if (this.y > stage.stageHeight) this.y = 0;
		}
	}
}