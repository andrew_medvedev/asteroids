package game.objects {
	import flash.display.Shape;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class ShipBullet extends Sprite {
		static const SPEED:Number = 14;
		
		private var shape:Shape;
		
		public var active:Boolean = false;
		private var inactiveCallback:Function;
		
		private var life:Number = 1;
		private var lifeDecr:Number = .04;
		
		private var _movVectorX:Number = 0;
		private var _movVectorY:Number = 0;
		
		public function ShipBullet(_inactiveCallback:Function) {
			shape = new Shape();
			addChild(shape);
			shape.graphics.beginFill(0xffffff);
			shape.graphics.drawCircle(0, 0, 1);
			shape.graphics.endFill();
			shape.visible = false;
			
			inactiveCallback = _inactiveCallback;
		}
		public function mobilize():void {
			active = shape.visible = true;
			life = 1;
			_movVectorX = Math.cos(this.rotation * Math.PI / 180) * SPEED;
			_movVectorY = Math.sin(this.rotation * Math.PI / 180) * SPEED;
		}
		public function destroy():void {
			active = shape.visible = false;
			_movVectorX = 0;
			_movVectorY = 0;
			inactiveCallback();
		}
		public function step():void {
			if (active) {
				life -= lifeDecr;
				if (life <= 0)
					destroy();
				else {
					this.x += _movVectorX;
					this.y += _movVectorY;
					if (this.x < 0) this.x = stage.stageWidth;
					if (this.x > stage.stageWidth) this.x = 0;
					if (this.y < 0) this.y = stage.stageHeight;
					if (this.y > stage.stageHeight) this.y = 0;
				}
			}
		}
	}
}