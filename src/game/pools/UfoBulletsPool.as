package game.pools {
	import flash.display.Sprite;
	import game.objects.UfoBullet;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class UfoBulletsPool {
		
		public var bullets:Vector.<UfoBullet>;
		
		public var numOfReady:int;
		
		public var mobXTrigger:Number = 0;
		public var mobYTrigger:Number = 0;
		public var mobRotTrigger:Number = 0;
		
		private var _i:int;
		public var _i2:int;
		private var _tempBullet:UfoBullet;
		
		public function UfoBulletsPool() {
			bullets = new Vector.<UfoBullet>;
		}
		public function initPool(_numOfCopies:int, _cont:Sprite):void {
			for (var i:int = 0; i < _numOfCopies; i++ ) {
				var _bullet:UfoBullet = new UfoBullet(inactCallback);
				_cont.addChild(_bullet);
				bullets[bullets.length] = _bullet;
			}
			_i2 = bullets.length;
			numOfReady = _numOfCopies;
		}
		public function mobilizeBullet():Boolean {
			if (numOfReady == 0) return false;
			else {
				numOfReady--;
				for (_i = 0; _i < _i2; _i++ ) {
					_tempBullet = bullets[_i];
					if (!_tempBullet.active) {
						_tempBullet.x = mobXTrigger;
						_tempBullet.y = mobYTrigger;
						_tempBullet.rotation = mobRotTrigger;
						_tempBullet.mobilize();
						return true;
					}
				}
			}
			return false;
		}
		private function inactCallback():void {
			numOfReady++;
		}
		public function step():void {
			for (_i = 0; _i < _i2; _i++ )
				bullets[_i].step();
		}
	}
}