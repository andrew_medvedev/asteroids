package game.pools {
	import flash.display.Sprite;
	import game.managing.BitmapStorage;
	import game.objects.Asteroid;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class AsteroidPool {
		static const ROT_DEVIANT:Number = 30;
		static const SPAWN_BORDER:Number = 150;
		
		public var asteroids:Vector.<Asteroid>;
		
		public var mobXTrigger:Number = 0;
		public var mobYTrigger:Number = 0;
		public var mobRotTrigger:Number = 0;
		
		public var onAsteroidInactive:Function;
		
		private var bmds:BitmapStorage;
		
		private var _i:int;
		public var _i2:int;
		private var _tempAster:Asteroid;
		
		public function AsteroidPool(_bmds:BitmapStorage) {
			bmds = _bmds;
			asteroids = new Vector.<Asteroid>;
		}
		public function appendPool(_numOfCopies:int, _cont:Sprite):void {
			for (_i = 0; _i < _numOfCopies ; _i++ ) {
				var _aster:Asteroid = new Asteroid(onInactiveCallback, bmds);
				asteroids[asteroids.length] = _aster;
				_cont.addChild(_aster);
			}
			_i2 = asteroids.length;
		}
		public function mobilizeAsteroid(_size:int):void {
			for (_i = 0; _i < _i2; _i++ ) {
				_tempAster = asteroids[_i];
				if (!_tempAster.active) {
					if (_size == 3) {
						if (Math.round(Math.random()))
							_tempAster.x = Math.random() * SPAWN_BORDER;
						else _tempAster.x = _tempAster.stage.stageWidth - SPAWN_BORDER + Math.random() * SPAWN_BORDER;
						if (Math.round(Math.random()))
							_tempAster.y = Math.random() * SPAWN_BORDER;
						else _tempAster.y = _tempAster.stage.stageHeight - SPAWN_BORDER + Math.random() * SPAWN_BORDER;
						_tempAster.mobilize(_size, Math.random() * 360);
					} else {
						_tempAster.x = mobXTrigger;
						_tempAster.y = mobYTrigger;
						_tempAster.mobilize(_size, mobRotTrigger - ROT_DEVIANT + Math.random() * ROT_DEVIANT * 2);
					}
					return;
				}
			}
		}
		private function onInactiveCallback(_ast:Asteroid):void {
			onAsteroidInactive(_ast);
		}
		public function step():void {
			for (_i = 0; _i < _i2; _i++ )
				asteroids[_i].step();
		}
	}
}