package game.lays {
	import flash.display.Shape;
	import flash.geom.Point;
	import game.managing.LinksHolder;
	import game.objects.Asteroid;
	import game.objects.Ufo;
	import game.pools.AsteroidPool;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class LaserLay extends Shape {
		static const CHECK_STEP:Number = 5;
		
		private var wid:Number;
		private var hei:Number;
		
		public var xTrigger:Number = 0;
		public var yTrigger:Number = 0;
		public var rotTrigger:Number = 0;
		
		private var links:LinksHolder;
		
		private var goingOut:Number = 1;
		private var goingOutSpeed:Number = .2;
		private var goingOutTrigger:Boolean = false;
		
		public var reloading:Number = 1;
		private var reloadingSpeed:Number = .015;
		private var reloadingTrigger:Boolean = false;
		
		private var _check:Point;
		private var _checkXVector:Number = 0;
		private var _checkYVector:Number = 0;
		
		private var _i:int;
		private var _tempAster:Asteroid;
		
		public function LaserLay(_wid:Number, _hei:Number, _links:LinksHolder) {
			wid = _wid;
			hei = _hei;
			links = _links;
			_check = new Point();
		}
		public function spawnLaser():void {
			if (!reloadingTrigger) {
				_check.x = xTrigger;
				_check.y = yTrigger;
				_checkXVector = Math.cos(rotTrigger * Math.PI / 180) * CHECK_STEP;
				_checkYVector = Math.sin(rotTrigger * Math.PI / 180) * CHECK_STEP;
				this.graphics.lineStyle(1, 0xffffff);
				this.graphics.moveTo(_check.x, _check.y);
				while (_check.x > 0 && _check.x < wid && _check.y > 0 && _check.y < hei) {
					_check.x += _checkXVector;
					_check.y += _checkYVector;
					this.graphics.lineTo(_check.x, _check.y);
					for (_i = 0; _i < links.asteroidsPool._i2; _i++ ) {
						_tempAster = links.asteroidsPool.asteroids[_i];
						if (_tempAster.active)
							if (getDistance(_tempAster.x, _tempAster.y, _check.x, _check.y) <= _tempAster.radius)
								_tempAster.destroy();
					}
					if (links.ufo.active)
						if (getDistance(links.ufo.x, links.ufo.y, _check.x, _check.y) <= links.ufo.radius)
								links.ufo.destroy();
				}
				goingOut = 1;
				goingOutTrigger = true;
				reloadingTrigger = true;
				reloading = 0;
				links.guiLay.laserBar.tip.visible = false;
			}
		}
		private function getDistance(_x1:Number, _y1:Number, _x2:Number, _y2:Number):Number {
			return Math.sqrt((_y2 - _y1) * (_y2 - _y1) + (_x2 - _x1) * (_x2 - _x1))
		}
		public function step():void {
			if (goingOutTrigger) {
				goingOut -= goingOutSpeed
				if (goingOut <= 0) {
					goingOutTrigger = false;
					this.graphics.clear();
				}
			}
			if (reloadingTrigger) {
				reloading += reloadingSpeed;
				if (reloading >= 1) {
					reloadingTrigger = false;
					links.guiLay.laserBar.Bar = 1;
					links.guiLay.laserBar.tip.visible = true;
				} else links.guiLay.laserBar.Bar = reloading;
			}
		}
	}
}