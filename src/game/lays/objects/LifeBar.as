package game.lays.objects {
	import flash.display.Shape;
	import flash.display.Sprite;
	import game.factories.ShapeFactory;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class LifeBar extends Sprite {
		
		private var lifes:Vector.<Shape>;
		
		public function LifeBar(_numOfLifes:int = 3) {
			lifes = new Vector.<Shape>;
			var _x:Number = 0;
			for (var i:int = 0; i < _numOfLifes; i++ ) {
				var _l:Shape = new Shape();
				ShapeFactory.drawGuiShip(_l);
				addChild(_l);
				lifes[lifes.length] = _l;
				_l.x = _x;
				_x += _l.width + 5;
			}
		}
		public function destroyLife():int {
			removeChild(lifes[lifes.length - 1]);
			lifes.splice(lifes.length - 1, 1);
			return lifes.length;
		}
	}
}