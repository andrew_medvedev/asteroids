package game.lays.objects {
	import flash.display.Sprite;
	import flash.text.TextField;
	import game.factories.OtherPlushes;
	import game.factories.TextFieldFactory;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class GameOverCont extends Sprite {
		
		private var go:TextField;
		private var scores:TextField;
		
		private var tip:TextField;
		
		public function GameOverCont(_scores:int) {
			go = new TextField();
			TextFieldFactory.setupTF(go, "GAME OVER", OtherPlushes.textFormat_30);
			addChild(go);
			go.x = -go.width * .5;
			
			scores = new TextField();
			TextFieldFactory.setupTF(scores, "SCORE: " + _scores, OtherPlushes.textFormat_18);
			addChild(scores);
			scores.x = -scores.width * .5;
			scores.y = go.height + 10;
			
			tip = new TextField();
			TextFieldFactory.setupTF(tip, "Press <Space>", OtherPlushes.textFormat_12);
			addChild(tip);
			tip.x = -tip.width * .5;
			tip.y = scores.y + scores.height + 40;
		}
	}
}