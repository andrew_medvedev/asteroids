package game.lays.objects {
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.text.TextField;
	import game.factories.OtherPlushes;
	import game.factories.TextFieldFactory;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class LaserBar extends Sprite {
		
		private var frame:Shape;
		private var bar:Shape;
		
		public var tip:TextField;
		
		public function LaserBar() {
			frame = new Shape();
			addChild(frame);
			frame.graphics.lineStyle(1, 0xffffff);
			frame.graphics.drawRect(0, 0, 150, 7);
			
			bar = new Shape();
			addChild(bar);
			
			tip = new TextField();
			TextFieldFactory.setupTF(tip, "Ctrl for laser", OtherPlushes.textFormat_12);
			addChild(tip);
			tip.y = 10;
			tip.x = (150 - tip.width) / 2;
			
			Bar = 1;
		}
		public function set Bar(value:Number):void {
			bar.graphics.clear();
			bar.graphics.beginFill(0xffffff);
			bar.graphics.drawRect(3, 3, value * 145, 1);
			bar.graphics.endFill();
		}
	}
}