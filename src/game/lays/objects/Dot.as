package game.lays.objects {
	import flash.display.Shape;
	import game.factories.MathPlushies;
	import game.lays.objects.struct.Gfx;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class Dot extends Gfx {
		static const SPEED_FROM:Number = .6;
		static const SPEED_TO:Number = 1.5;
		
		static const LIFE_DECR_FROM:Number = .02;
		static const LIFE_DECR_TO:Number = .07;
		
		private var shape:Shape;
		
		private var movXVec:Number = 0;
		private var movYVec:Number = 0;
		
		public function Dot() {
			shape = new Shape();
			addChild(shape);
			shape.graphics.beginFill(0xffffff);
			shape.graphics.drawCircle(0, 0, 1);
			shape.graphics.endFill();
			
			var _dir:Number = Math.random() * 360;
			var _sp:Number = MathPlushies.getRandom(SPEED_FROM, SPEED_TO);
			lifeDecrement = MathPlushies.getRandom(LIFE_DECR_FROM, LIFE_DECR_TO);
			
			movXVec = Math.cos(_dir * Math.PI / 180) * _sp;
			movYVec = Math.sin(_dir * Math.PI / 180) * _sp;
			
			this.x += movXVec * 10;
			this.y += movYVec * 10;
		}
		public override function step():void {
			life -= lifeDecrement;
			if (life <= 0)
				disposeMe = true;
			else {
				this.x += movXVec;
				this.y += movYVec;
			}
		}
	}
}