package game.lays.objects {
	import flash.text.TextField;
	import game.factories.OtherPlushes;
	import game.factories.TextFieldFactory;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class Scores extends TextField {
		
		public function Scores() {
			TextFieldFactory.setupTF(this, "0", OtherPlushes.textFormat_18);
		}
		public function get Score():int {
			return int(this.text);
		}
		public function set Score(value:int):void {
			this.text = String(value);
			this.width = this.textWidth + 5;
		}
	}
}