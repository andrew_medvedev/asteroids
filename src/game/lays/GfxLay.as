package game.lays {
	import flash.display.Sprite;
	import game.lays.objects.Dot;
	import game.lays.objects.Stick;
	import game.lays.objects.struct.Gfx;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class GfxLay extends Sprite {
		
		private var gfxs:Vector.<Gfx>;
		
		public var howMuchTrigger:int = 0;
		public var xTrigger:Number = 0;
		public var yTrigger:Number = 0;
		
		private var _i:int;
		private var _i2:int;
		private var _tmpGfx:Gfx;
		
		public function GfxLay() {
			gfxs = new Vector.<Gfx>;
		}
		public function spawnDots():void {
			for (_i = 0; _i < howMuchTrigger; _i++ ) {
				var _dot:Dot = new Dot();
				addChild(_dot);
				_dot.x = xTrigger;
				_dot.y = yTrigger;
				gfxs[gfxs.length] = _dot;
			}
		}
		public function spawnSticks():void {
			for (_i = 0; _i < howMuchTrigger; _i++ ) {
				var _st:Stick = new Stick();
				addChild(_st);
				_st.x = xTrigger;
				_st.y = yTrigger;
				gfxs[gfxs.length] = _st;
			}
		}
		public function step():void {
			_i2 = gfxs.length;
			for (_i = 0; _i < _i2; _i++ ) {
				_tmpGfx = gfxs[_i];
				_tmpGfx.step();
				if (_tmpGfx.disposeMe) {
					removeChild(_tmpGfx);
					gfxs.splice(_i--, 1);
					_i2--;
				}
			}
		}
	}
}