package game.lays {
	import flash.display.Sprite;
	import game.lays.objects.GameOverCont;
	import game.lays.objects.LaserBar;
	import game.lays.objects.LifeBar;
	import game.lays.objects.Scores;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class GuiLay extends Sprite {
		
		public var scores:Scores;
		public var lifes:LifeBar;
		public var laserBar:LaserBar;
		
		public var gameOver:GameOverCont;
		
		public function init():void {
			scores = new Scores();
			addChild(scores);
			scores.x = 20;
			scores.y = 10;
			
			lifes = new LifeBar();
			addChild(lifes);
			lifes.x = 20;
			lifes.y = scores.y + scores.height + 5;
			
			laserBar = new LaserBar();
			addChild(laserBar);
			laserBar.x = stage.stageWidth - laserBar.width - 25;
			laserBar.y = stage.stageHeight - laserBar.height - 10;
		}
		public function spawnGameOver():void {
			removeChild(scores);
			removeChild(lifes);
			removeChild(laserBar);
			gameOver = new GameOverCont(scores.Score);
			addChild(gameOver);
			gameOver.x = stage.stageWidth * .5;
			gameOver.y = (stage.stageHeight - gameOver.height) * .5;
		}
	}
}