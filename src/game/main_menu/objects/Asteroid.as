package game.main_menu.objects {
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import game.factories.MathPlushies;
	import game.factories.ShapeFactory;
	import game.managing.BitmapStorage;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class Asteroid extends Sprite {
		static const LARGE_SPEED_FROM:Number = 1;
		static const LARGE_SPEED_TO:Number = 2;
		static const MEDIUM_SPEED_FROM:Number = 2;
		static const MEDIUM_SPEED_TO:Number = 3;
		static const THE_TINY_SPEED_FROM:Number = 3;
		static const THE_TINY_SPEED_TO:Number = 4;
		
		public var graphix:Bitmap;
		public var shape:Shape;
		
		private var movVectorX:Number = 0;
		private var movVectorY:Number = 0;
		
		public function Asteroid(_bmds:BitmapStorage) {
			graphix = new Bitmap(_bmds.AsteroidBmd, "auto", true);
			addChild(graphix);
			graphix.visible = false;
			shape = new Shape();
			addChild(shape);
			var _sp:Number;
			switch(int(Math.random() * 3)) {
				case 0: 
					ShapeFactory.drawAsteroid(shape, 6); 
					_sp = MathPlushies.getRandom(LARGE_SPEED_FROM, LARGE_SPEED_TO);
					break;
				case 1: 
					ShapeFactory.drawAsteroid(shape, 4); 
					_sp = MathPlushies.getRandom(MEDIUM_SPEED_FROM, MEDIUM_SPEED_TO);
					graphix.scaleX = graphix.scaleY = .66;
					break;
				case 2: 
					ShapeFactory.drawAsteroid(shape, 2.2); 
					_sp = MathPlushies.getRandom(THE_TINY_SPEED_FROM, THE_TINY_SPEED_TO);
					graphix.scaleX = graphix.scaleY = .38;
					break;
			}
			graphix.x = -graphix.width * .5;
			graphix.y = -graphix.height * .5;
			var _rot:Number = Math.random() * 360;
			movVectorX = Math.cos(_rot * Math.PI / 180) * _sp;
			movVectorY = Math.sin(_rot * Math.PI / 180) * _sp;
		}
		public function step():void {
			this.x += movVectorX;
			this.y += movVectorY;
			if (this.x < 0) this.x = stage.stageWidth;
			if (this.x > stage.stageWidth) this.x = 0;
			if (this.y < 0) this.y = stage.stageHeight;
			if (this.y > stage.stageHeight) this.y = 0;
		}
	}
}