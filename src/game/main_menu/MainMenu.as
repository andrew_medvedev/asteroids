package game.main_menu {
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import flash.text.TextField;
	import game.factories.MathPlushies;
	import game.factories.OtherPlushes;
	import game.factories.TextFieldFactory;
	import game.main_menu.objects.Asteroid;
	import game.managing.BitmapStorage;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class MainMenu extends Sprite {
		static const NUM_OF_ASTEROIDS_FROM:int = 6;
		static const NUM_OF_ASTEROIDS_TO:int = 12;
		
		private var asteroids:Vector.<Asteroid>;
		
		private var head:TextField;
		private var tip:TextField;
		private var qTip:TextField;
		
		private var background:Bitmap;
		private var niceGraphix:Boolean = false;
		
		public var onPlay:Function;
		
		private var _i:int;
		private var _i2:int;
		
		private var bmds:BitmapStorage;
		
		public function MainMenu(_bmds:BitmapStorage) {
			asteroids = new Vector.<Asteroid>;
			bmds = _bmds;
		}
		public function init(_niceGraphix:Boolean):void {
			this.graphics.beginFill(0);
			this.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			this.graphics.endFill();
			background = new Bitmap(bmds.background);
			addChild(background);
			background.visible = false;
			_i2 = int(MathPlushies.getRandom(NUM_OF_ASTEROIDS_FROM, NUM_OF_ASTEROIDS_TO));
			for (var i:int = 0; i < _i2; i++ ) {
				var _ast:Asteroid = new Asteroid(bmds);
				addChild(_ast);
				_ast.x = stage.stageWidth * Math.random();
				_ast.y = stage.stageHeight * Math.random();
				asteroids[asteroids.length] = _ast;
			}
			head = new TextField();
			TextFieldFactory.setupTF(head, "ASTEROIDS", OtherPlushes.textFormat_70);
			addChild(head);
			head.x = (stage.stageWidth - head.width) * .5;
			head.y = 160;
			tip = new TextField();
			TextFieldFactory.setupTF(tip, "Press <Space> to play", OtherPlushes.textFormat_18);
			addChild(tip);
			tip.x = (stage.stageWidth - tip.width) * .5;
			tip.y = 300;
			
			qTip = new TextField();
			TextFieldFactory.setupTF(qTip, "Press <Q> to change graphix", OtherPlushes.textFormat_12);
			addChild(qTip);
			qTip.x = (stage.stageWidth - qTip.width) * .5;
			qTip.y = 400;
			
			if (_niceGraphix) changeGraphix();
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, kDo);
		}
		public function kDo(e:KeyboardEvent):void {
			switch(e.keyCode) {
				case 32: onPlay(niceGraphix); break;
				case 81: changeGraphix(); break;
			}
		}
		public function dispose():void {
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, kDo);
		}
		private function changeGraphix():void {
			niceGraphix = background.visible = !niceGraphix;
			for (_i = 0; _i < _i2; _i++ ) {
				asteroids[_i].graphix.visible = niceGraphix;
				asteroids[_i].shape.visible = !niceGraphix;
			}
		}
		public function step():void {
			for (_i = 0; _i < _i2; _i++ )
				asteroids[_i].step();
		}
	}
}