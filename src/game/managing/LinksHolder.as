package game.managing {
	import flash.display.Sprite;
	import game.lays.GfxLay;
	import game.lays.GuiLay;
	import game.lays.LaserLay;
	import game.objects.Ship;
	import game.objects.Ufo;
	import game.pools.AsteroidPool;
	import game.pools.ShipBulletsPool;
	import game.pools.UfoBulletsPool;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class LinksHolder {
		
		public var ship:Ship;
		public var ufo:Ufo;
		
		public var shipBulletsPool:ShipBulletsPool;
		public var asteroidsPool:AsteroidPool;
		public var ufoBulletsPool:UfoBulletsPool;
		
		public var objectsCont:Sprite;
		public var guiLay:GuiLay;
		public var laserLay:LaserLay;
		public var gfxLay:GfxLay;
		
		public var bmdStorage:BitmapStorage;
		
	}
}