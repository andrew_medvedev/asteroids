package game.managing {
	import flash.display.Sprite;
	import game.lays.GuiLay;
	import game.objects.Asteroid;
	import game.objects.Ship;
	import game.objects.Ufo;
	import game.pools.AsteroidPool;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class GameplayManager {
		
		private var links:LinksHolder;
		
		private var ufoReloading:Number = 0;
		private var ufoReloadingSpeed:Number = .001;
		
		private var numOfAsteroids:int = 5;
		
		private var numOfTargets:int;
		
		private var reloading:Number = 0;
		private var reloadingSpeed:Number = .02;
		private var reloadingTrigger:Boolean = false;
		
		private var _i:int;
		
		public function GameplayManager(_links:LinksHolder) {
			links = _links;
			links.asteroidsPool.appendPool(20, links.objectsCont);
			links.asteroidsPool.onAsteroidInactive = onAsteroidDestroy;
			
			links.ship.onNoLifes = endGame;
		}
		public function newRound():void {
			for (_i = 0 ; _i < numOfAsteroids; _i++ )
				links.asteroidsPool.mobilizeAsteroid(Asteroid.LARGE);
			numOfTargets = numOfAsteroids * 4;
		}
		public function onAsteroidDestroy(_ast:Asteroid):void {
			switch(_ast.size) {
				case Asteroid.LARGE:
					links.asteroidsPool.mobRotTrigger = _ast.direction;
					links.asteroidsPool.mobXTrigger = _ast.x;
					links.asteroidsPool.mobYTrigger = _ast.y;
					links.asteroidsPool.mobilizeAsteroid(2);
					links.asteroidsPool.mobilizeAsteroid(2);
					links.guiLay.scores.Score += 20;
					break;
				case Asteroid.MEDIUM:
					links.asteroidsPool.mobRotTrigger = _ast.direction;
					links.asteroidsPool.mobXTrigger = _ast.x;
					links.asteroidsPool.mobYTrigger = _ast.y;
					links.asteroidsPool.mobilizeAsteroid(1);
					links.asteroidsPool.mobilizeAsteroid(1);
					links.guiLay.scores.Score += 50;
					break;
				case Asteroid.THE_TINY:
					links.guiLay.scores.Score += 100;
					if (--numOfTargets == 0) {
						reloading = 0;
						reloadingTrigger = true;
					}
					break;
			}
		}
		private function endGame():void {
			links.guiLay.spawnGameOver();
		}
		public function step():void {
			if (reloadingTrigger) {
				reloading += reloadingSpeed;
				if (reloading >= 1) {
					reloadingTrigger = false;
					numOfAsteroids++;
					links.asteroidsPool.appendPool(4, links.objectsCont);
					newRound();
				}
			}
			ufoReloading += ufoReloadingSpeed;
			if (ufoReloading >= 1) {
				ufoReloading--;
				switch(int(Math.random() * 4)) {
					case 0:
						links.ufo.x = 0;
						links.ufo.y = Math.random() * links.ufo.stage.stageHeight;
						break;
					case 1:
						links.ufo.x = Math.random() * links.ufo.stage.stageWidth;
						links.ufo.y = 0;
						break;
					case 2:
						links.ufo.x = links.ufo.stage.stageWidth;
						links.ufo.y = Math.random() * links.ufo.stage.stageHeight;
						break;
					case 3:
						links.ufo.x = Math.random() * links.ufo.stage.stageWidth;
						links.ufo.y = links.ufo.stage.stageHeight;
						break;
				}
				links.ufo.mobilize();
			}
		}
	}
}