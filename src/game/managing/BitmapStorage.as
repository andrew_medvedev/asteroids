package game.managing {
	import flash.display.BitmapData;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class BitmapStorage {
		
		public var background:BitmapData;
		
		private var aster1:BitmapData;
		private var aster2:BitmapData;
		private var aster3:BitmapData;
		private var aster4:BitmapData;
		
		public var ship:BitmapData;
		
		public var ufo1:BitmapData;
		public var ufo2:BitmapData;
		public var ufo3:BitmapData;
		public var ufo4:BitmapData;
		public var ufo5:BitmapData;
		public var ufo6:BitmapData;
		public var ufo7:BitmapData;
		public var ufo8:BitmapData;
		public var ufo9:BitmapData;
		public var ufo10:BitmapData;
		public var ufo11:BitmapData;
		public var ufo12:BitmapData;
		
		public function BitmapStorage() {
			background = new BackgroundBmd;
			
			aster1 = new AsteroidBmd1;
			aster2 = new AsteroidBmd2;
			aster3 = new AsteroidBmd3;
			aster4 = new AsteroidBmd4;
			
			ship = new ShipBmd;
			
			ufo1 = new UfoBmd1;
			ufo2 = new UfoBmd2;
			ufo3 = new UfoBmd3;
			ufo4 = new UfoBmd4;
			ufo5 = new UfoBmd5;
			ufo6 = new UfoBmd6;
			ufo7 = new UfoBmd7;
			ufo8 = new UfoBmd8;
			ufo9 = new UfoBmd9;
			ufo10 = new UfoBmd10;
			ufo11 = new UfoBmd11;
			ufo12 = new UfoBmd12;
		}
		public function get AsteroidBmd():BitmapData {
			switch(int(Math.random() * 4)) {
				case 0: return aster1;
				case 1: return aster2;
				case 2: return aster3;
				case 3: return aster4;
			}
			return null;
		}
	}
}