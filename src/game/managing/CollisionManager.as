package game.managing {
	import game.lays.GfxLay;
	import game.objects.Asteroid;
	import game.objects.Ship;
	import game.objects.ShipBullet;
	import game.objects.Ufo;
	import game.objects.UfoBullet;
	import game.pools.AsteroidPool;
	import game.pools.ShipBulletsPool;
	import game.pools.UfoBulletsPool;
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class CollisionManager {
		
		private var links:LinksHolder;
		
		private var _i:int;
		private var _tempBul:ShipBullet;
		private var _j:int;
		private var _tempAster:Asteroid;
		private var _tempUfoBul:UfoBullet;
		
		public function CollisionManager(_links:LinksHolder) {
			links = _links;
		}
		public function step():void {
			if (links.ship.alive) {
				if (links.ufo.active)
					if (getDistance(links.ship.x, links.ship.y, links.ufo.x, links.ufo.y) <= (links.ufo.radius + links.ship.radius)) {
						shipBoom();
						links.ship.destroy();
						links.gfxLay.xTrigger = links.ufo.x;
						links.gfxLay.yTrigger = links.ufo.y;
						links.gfxLay.howMuchTrigger = 10;
						links.gfxLay.spawnDots();
						links.ufo.destroy();
					}
				if (links.ufoBulletsPool.numOfReady < links.ufoBulletsPool._i2)
				for (_i = 0; _i < links.ufoBulletsPool._i2; _i++ ) {
					_tempUfoBul = links.ufoBulletsPool.bullets[_i];
					if (_tempUfoBul.active)
						if (getDistance(_tempUfoBul.x, _tempUfoBul.y, links.ship.x, links.ship.y) <= links.ship.radius) {
							shipBoom();
							links.ship.destroy();
							_tempUfoBul.destroy();
						}
				}
			}
			for (_i = 0; _i < links.asteroidsPool._i2; _i++ ) {
				_tempAster = links.asteroidsPool.asteroids[_i];
				if (_tempAster.active) {
					if (links.ship.alive) if (getDistance(links.ship.x, links.ship.y, _tempAster.x, _tempAster.y) <= (_tempAster.radius + links.ship.radius)) {
						asteroidBoom();
						_tempAster.destroy();
						shipBoom();
						links.ship.destroy();
						continue;
					}
					if (links.shipBulletsPool.numOfReady < links.shipBulletsPool._i2)
						for (_j = 0 ; _j < links.shipBulletsPool._i2; _j++ ) {
							_tempBul = links.shipBulletsPool.bullets[_j];
							if (_tempBul.active)
								if (getDistance(_tempBul.x, _tempBul.y, _tempAster.x, _tempAster.y) <= _tempAster.radius) {
									_tempBul.destroy();
									asteroidBoom();
									_tempAster.destroy();
								}
						}
					}
			}
			if (links.ufo.active)
				if (links.shipBulletsPool.numOfReady < links.shipBulletsPool._i2)
					for (_i = 0 ; _i < links.shipBulletsPool._i2; _i++ ) {
						_tempBul = links.shipBulletsPool.bullets[_i];
						if (_tempBul.active)
							if (getDistance(_tempBul.x, _tempBul.y, links.ufo.x, links.ufo.y) <= links.ufo.radius) {
								_tempBul.destroy();
								links.gfxLay.xTrigger = links.ufo.x;
								links.gfxLay.yTrigger = links.ufo.y;
								links.gfxLay.howMuchTrigger = 10;
								links.gfxLay.spawnDots();
								links.ufo.destroy();
							}
					}
		}
		private function shipBoom():void {
			links.gfxLay.xTrigger = links.ship.x;
			links.gfxLay.yTrigger = links.ship.y;
			links.gfxLay.howMuchTrigger = 5;
			links.gfxLay.spawnSticks();
		}
		private function asteroidBoom():void {
			links.gfxLay.xTrigger = _tempAster.x;
			links.gfxLay.yTrigger = _tempAster.y;
			links.gfxLay.howMuchTrigger = 10;
			links.gfxLay.spawnDots();
		}
		private function getDistance(_x1:Number, _y1:Number, _x2:Number, _y2:Number):Number {
			return Math.sqrt((_y2 - _y1) * (_y2 - _y1) + (_x2 - _x1) * (_x2 - _x1))
		}
	}
}