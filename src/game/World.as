package game {
	import flash.display.Bitmap;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	import game.factories.AnimationsFac;
	import game.factories.ShapeFactory;
	import game.lays.GfxLay;
	import game.lays.GuiLay;
	import game.lays.LaserLay;
	import game.managing.BitmapStorage;
	import game.managing.CollisionManager;
	import game.managing.GameplayManager;
	import game.managing.LinksHolder;
	import game.objects.Ship;
	import game.objects.Ufo;
	import game.pools.AsteroidPool;
	import game.pools.ShipBulletsPool;
	import game.pools.UfoBulletsPool;
	
	/**
	 * ...
	 * @author Andrew Rahimov
	 */
	public final class World extends Sprite {
		
		// objects
		private var ship:Ship;
		private var ufo:Ufo;
		
		// conts & lays
		private var objectsCont:Sprite;
		private var gui:GuiLay;
		private var laser:LaserLay;
		private var gfx:GfxLay;
		
		// pools
		private var shipBulletsPool:ShipBulletsPool;
		private var asteroids:AsteroidPool;
		private var ufoBullets:UfoBulletsPool;
		
		// managing
		private var gameplay:GameplayManager;
		private var collisions:CollisionManager;
		private var bmds:BitmapStorage;
		private var holder:LinksHolder;
		
		// callback
		public var onGameOver:Function;
		
		// etc
		private var bg:Bitmap;
		private var niceGraphix:Boolean = false;
		
		public function World(_bmds:BitmapStorage):void {
			bmds = _bmds;
		}
		public function init(_niceGraphix:Boolean):void {
			holder = new LinksHolder;
			holder.bmdStorage = bmds;
			
			initGraphics();
			initListeners();
			initPools();
			initObjects();
			initGui();
			initManaging();
			
			if (_niceGraphix) changeGraphix();
		}
		private function initGraphics():void {
			bg = new Bitmap(bmds.background);
			addChild(bg);
			bg.visible = false;
			this.graphics.beginFill(0);
			this.graphics.drawRect(0, 0, this.stage.stageWidth, this.stage.stageHeight);
			this.graphics.endFill();
			objectsCont = new Sprite();
			addChild(objectsCont);
		}
		private function initListeners():void {
			stage.addEventListener(KeyboardEvent.KEY_DOWN, kDo);
			stage.addEventListener(KeyboardEvent.KEY_UP, kUp);
		}
		private function initPools():void {
			shipBulletsPool = new ShipBulletsPool();
			shipBulletsPool.initPool(4, objectsCont);
			asteroids = new AsteroidPool(bmds);
			ufoBullets = new UfoBulletsPool();
			ufoBullets.initPool(2, objectsCont);
			
			holder.asteroidsPool = asteroids;
			holder.shipBulletsPool = shipBulletsPool;
			holder.ufoBulletsPool = ufoBullets;
		}
		private function initObjects():void {
			objectsCont = new Sprite();
			addChild(objectsCont);
			ship = new Ship(holder);
			objectsCont.addChild(ship);
			ship.x = stage.stageWidth * .5;
			ship.y = stage.stageHeight * .5;
			ship.rotation -= 90;
			ufo = new Ufo(holder);
			objectsCont.addChild(ufo);
			laser = new LaserLay(stage.stageWidth, stage.stageHeight, holder);
			addChild(laser);
			gfx = new GfxLay();
			addChild(gfx);
			
			holder.ship = ship;
			holder.ufo = ufo;
			holder.gfxLay = gfx;
			holder.laserLay = laser;
			holder.objectsCont = objectsCont;
		}
		private function initGui():void {
			gui = new GuiLay();
			addChild(gui);
			gui.init();
			
			holder.guiLay = gui;
		}
		private function initManaging():void {
			gameplay = new GameplayManager(holder);
			gameplay.newRound();
			collisions = new CollisionManager(holder);
		}
		private function kDo(e:KeyboardEvent):void {
			if (ship.alive) switch(e.keyCode) {
				case 38: ship.forward = true; break;
				case 37: ship.left = true; break;
				case 39: ship.right = true; break;
				case 32: ship.fire = true; break;
				case 17: 
					laser.xTrigger = ship.x;
					laser.yTrigger = ship.y;
					laser.rotTrigger = ship.rotation;
					laser.spawnLaser();
					break;
			}
			if (gui.gameOver) 
				if (e.keyCode == 32) 
					onGameOver(niceGraphix);
			if (e.keyCode == 81) changeGraphix();
		}
		private function kUp(e:KeyboardEvent):void {
			if (ship.alive) switch(e.keyCode) {
				case 38: ship.forward = false; break;
				case 37: ship.left = false; break;
				case 39: ship.right = false; break;
				case 32: ship.fire = false; break;
			}
		}
		private function changeGraphix():void {
			niceGraphix = bg.visible = !niceGraphix;
			ship.shape.visible = !niceGraphix;
			ship.graphix.visible = niceGraphix;
			ufo.shape.visible = !niceGraphix;
			ufo.graphix.visible = ufo.graphix.play = niceGraphix;
			for (var i:int = 0; i < asteroids._i2; i++ ) {
				asteroids.asteroids[i].shape.visible = !niceGraphix;
				asteroids.asteroids[i].graphix.visible = niceGraphix;
			}
		}
		public function dispose():void {
			stage.removeEventListener(KeyboardEvent.KEY_DOWN, kDo);
			stage.removeEventListener(KeyboardEvent.KEY_UP, kUp);
			AnimationsFac.staticDisposePack(ufo.graphix);
		}
		public function step():void {
			ship.step();
			shipBulletsPool.step();
			asteroids.step();
			collisions.step();
			gameplay.step();
			ufo.step();
			ufoBullets.step();
			laser.step();
			gfx.step();
		}
	}
}